﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            if (!context.Contracts.Any())
            {

                context.AddRange(
                      new ContractBlock
                      {
                          Id = 1,
                          Type = "contract type",
                          Date = DateTime.Now,
                          ContractBody = "this is the body of the contract"
                      },
                new ContractBlock
                {
                    Id = 2,
                    Date = DateTime.Now,
                    Type = "contract type",
                   
                    ContractBody = "this is the body of the contract"
                },
                new ContractBlock
                {
                    Id = 3,
                    Type = "contract type",
                    Date = DateTime.Now,
                    ContractBody = "this is the body of the contract"
                },
                new ContractBlock
                {
                    Id = 4,
                    Type = "contract type",
                    Date = DateTime.Now,
                    ContractBody = "this is the body of the contract"
                });
                context.SaveChanges();

            };



        }

    }
}
