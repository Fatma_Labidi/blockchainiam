﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
   public  interface INodeRepository
    {


        IEnumerable<Node> GetAllNodes();
        Node GetNodeById(int NodeId);
         Byte[] GetPrivateKey(int IdNode);
        Byte[] GetPublicKey(int IdNode);


    }
}
