﻿using Sodium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class Node
    {
        public int Id { get; set; }
        public string Adress { get; set; }
        public string Num { get; set; }
        public String Owner { get; set; }
        public string Name { get; set; }
        public string State { get; set; }


        //TEST
      //  public KeyPair keyPair { get; set; }
        public Byte[] PublicKey { get; set; }
        public Byte[] PrivateKey { get; set; }

        //public   Byte[] PublicKey = PublicKeyAuth.GenerateKeyPair().PublicKey;
        //  public   Byte[] PrivateKey = PublicKeyAuth.GenerateKeyPair().PrivateKey;
    }
}
