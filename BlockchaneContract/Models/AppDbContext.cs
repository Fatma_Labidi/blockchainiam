﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlockchaneContract.Models
{
    public class AppDbContext : DbContext/*<IdentityUser>*/
    {
        public AppDbContext()
        {
        } 

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<ContractBlock> Contracts { get; set; }
        public DbSet<Block> BlockChain { get; set; }
        public DbSet<Block> ReceivedBlocks { get; set; }
        public DbSet<Node> Nodes { get; set; }
        public DbSet<User> User { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ContractBlock>()
                .HasOne(b => b.Blockchain)
                .WithMany(c => c.Contracts)
                .HasForeignKey(b => b.IdBlock)
                .HasConstraintName("ForeignKey_ContractBlock_BlockChain");

        //     modelBuilder.Entity<ContractBlock>()
        //        .HasOne(b => b.ReceivedBlocks)
        //        .WithMany(c => c.Contracts)
        //        .HasForeignKey(b => b.IdBlock)
        //        .HasConstraintName("ForeignKey_ContractBlock_ReceivedBlocks");
        }

    }

}
