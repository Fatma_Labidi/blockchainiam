﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class Block
    {
        public Guid Id { get; set; }
        public List<ContractBlock> Contracts { get; set; }
        public string Hash { get; set; }
        public string PreviousHash { get; set; }
        public DateTime Date { get; set; }
        public int NbrContracts { get; set; }
        public string MetaData { get; set; }
        public string  PublicKey { get; set; }
    }
}
