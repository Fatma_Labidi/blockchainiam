﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class NodeRepository : INodeRepository
    {

        private readonly AppDbContext _appDbContext;

        public NodeRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        

        public IEnumerable<Node> GetAllNodes()
        {
            return _appDbContext.Nodes;
        }

        public Node GetNodeById(int NodeId)
        {
            return _appDbContext.Nodes.FirstOrDefault(n => n.Id == NodeId);
        }



        //TEST
        public byte[] GetPrivateKey(int IdNode)
        {
            return _appDbContext.Nodes.FirstOrDefault(n => n.Id == IdNode).PrivateKey;
        }

        public byte[] GetPublicKey(int IdNode)
        {
            return _appDbContext.Nodes.FirstOrDefault(n => n.Id == IdNode).PublicKey;
        }

        

    }
}
