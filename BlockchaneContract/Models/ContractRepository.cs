﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class ContractRepository : IContractRepository
    {
        private readonly AppDbContext _appDbContext;

        public ContractRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public IEnumerable<ContractBlock> GetAllContracts()
        {
            return _appDbContext.Contracts;
        }
        
        public ContractBlock GetContractById(int contractId)
        {
            return _appDbContext.Contracts.LastOrDefault(c => c.Id == contractId);
        }

        public IEnumerable<ContractBlock> GetContractsByBlock(Guid idBlock)
        {

            return _appDbContext.Contracts.Where(c => c.IdBlock == idBlock);
        }

        public IEnumerable<ContractBlock> GetPendingContracts()
        {
            return _appDbContext.Contracts.Where(c => c.Signature.Length == 0);
        }
        public IEnumerable<ContractBlock> GetQueue()
        {
           
            return _appDbContext.Contracts.Where(c => (c.Signature.Length > 0 && c.IdBlock == null)).ToList();
        } 
    }
}
