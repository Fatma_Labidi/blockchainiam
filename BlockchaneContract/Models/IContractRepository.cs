﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
  public   interface IContractRepository
    {
        IEnumerable<ContractBlock> GetAllContracts();
        ContractBlock GetContractById(int contractId);
        IEnumerable<ContractBlock> GetContractsByBlock(Guid IdBlock);
        IEnumerable<ContractBlock> GetPendingContracts();
        IEnumerable<ContractBlock> GetQueue();

    }
}
