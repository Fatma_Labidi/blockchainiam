﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sodium;

namespace BlockchaneContract.Models
{
    public interface IBlockRepository
    {
        IEnumerable<Block> GetBlockChain();
        Block GetBlockChainById(Guid blockId);
        Block GetLastBlock();




    }

}
