﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class ContractBlock
    {
        public int Id { get; set; }
        public Guid? IdBlock { get; set; }
        public Block Blockchain { get; internal set; }
        public Guid Nym { get; set; }
        public string Version { get; set; }
        public string Type { get; set; }
        public string Signature { get; set; }
        public string Json { get; set; }
        public DateTime Date { get; set; }
        public string ContractBody { get; set; }
        public object ReceivedBlocks { get; internal set; }
    }
}
