﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sodium;

namespace BlockchaneContract.Models
{
    public class BlockRepository : IBlockRepository
    {

        private readonly AppDbContext _appDbContext;

        public BlockRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Block> GetBlockChain()
        {
            return _appDbContext.BlockChain;
        }

        public Block GetBlockChainById(Guid blockId)
        {
            return _appDbContext.BlockChain.FirstOrDefault(c => c.Id.Equals(blockId));

        }

        public Block GetLastBlock()
        {
            return _appDbContext.BlockChain.Last();
        }
    }
}
