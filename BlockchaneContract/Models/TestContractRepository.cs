﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.Models
{
    public class TestContractRepository : IContractRepository

    {
        private List<ContractBlock> _contracts;
        public TestContractRepository()
        {
            if (_contracts == null)
            {
                InitializeContracts();
            }
        }

        private void InitializeContracts()
        {
            _contracts = new List<ContractBlock>
                {
                new ContractBlock
                {
                    Id=1,
                    Type="contract type",
                    Date = DateTime.Now,
                    ContractBody ="this is the body of the contract"
                },
                new ContractBlock
                {
                    Id=2,
                    Type="contract type",
                    Date = DateTime.Now,
                    ContractBody ="this is the body of the contract"
                },
                new ContractBlock
                {
                    Id=3,
                    Date = DateTime.Now,
                    Type ="contract type",
                    ContractBody="this is the body of the contract"
                },
                new ContractBlock
                {
                    Id=4,
                    Type="contract type",
                    Date = DateTime.Now,

                    ContractBody ="this is the body of the contract"
                }
            };
        }


        public IEnumerable<ContractBlock> GetAllContracts()
        {
            return _contracts;
        }

        public ContractBlock GetContractById(int contractId)
        {
            return _contracts.FirstOrDefault(c => c.Id == contractId);
        }

        public IEnumerable<ContractBlock> GetContractsByBlock(Guid IdBlock)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ContractBlock> GetPendingContracts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ContractBlock> GetQueue()
        {
            throw new NotImplementedException();
        }
    }
}
