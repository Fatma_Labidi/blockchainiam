﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BlockchaneContract.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BlockchaneContract
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.Title = "IdentityServer";
                var host = new WebHostBuilder()
                     .UseKestrel()
                     .UseUrls("http://odin:5299", "http://192.168.1.100:5299", "http://localhost:5299")
                     .UseContentRoot(Directory.GetCurrentDirectory())
                     .UseIISIntegration()
                     .UseStartup<Startup>()
                     .Build();
                host.Run();
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static IWebHost BuildWebHost(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .Build();
    }
}
