﻿using BlockchaneContract.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;

namespace BlockchaneContract
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public static SqlConnection conn;
       public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
    .SetBasePath(env.ContentRootPath)
    .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
    .AddJsonFile($"appSettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
    .AddEnvironmentVariables();

            Configuration = builder.Build();
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
             conn = new SqlConnection("Server=(localdb)\\MSSQLLocalDB;Database=ContractsBlock;Trusted_Connection=True;MultipleActiveResultSets=true");

           // conn.Open();
            //   string conString = ConfigurationManager.ConnectionStrings("Server=(localdb)\\MSSQLLocalDB;Database=ContractsBlock;Trusted_Connection=True;MultipleActiveResultSets=true").ConnectionString.ToString();
            // using (var conn = new SqlConnection("Server=(localdb)\\MSSQLLocalDB;Database=ContractsBlock;Trusted_Connection=True;MultipleActiveResultSets=true"))

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(conn));

            // services.AddDbContext<AppDbContext>();
            //services.AddDbContext<AppDbContext>(options =>
           // options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IContractRepository, ContractRepository>();
            services.AddTransient<IBlockRepository, BlockRepository>();
            services.AddTransient<INodeRepository, NodeRepository>();
        
            //   conn.Open();
            
            //  IdentityServer4:
            try
            {
                JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
                services.AddAuthentication(options =>
                {
                    options.DefaultScheme = "Cookies";
                    options.DefaultChallengeScheme = "oidc";
                })
                    .AddCookie("Cookies")
                    .AddOpenIdConnect("oidc", options =>
                    {
                        options.SignInScheme = "Cookies";
                        options.Authority = "http://localhost:5000";
                        options.RequireHttpsMetadata = false;
                        options.ClientId = "Fatma";
                        options.ClientSecret = "secret";
                        options.ResponseType = "code id_token";
                        options.SaveTokens = true;
                        options.GetClaimsFromUserInfoEndpoint = true;
                    });
            }
            catch (Exception)
            {
            }
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();
             //app.UseIdentityServer();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Contracts}/{id?}");
            });
        }
    }
}
