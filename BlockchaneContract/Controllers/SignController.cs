﻿using BlockchaneContract.Models;
using BlockchaneContract.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

using System.IO;
using Sodium;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using BlockchaneContract;
using System.Security.Cryptography;

namespace blockchanecontract.controllers
{
    [Authorize]

    public class Signcontroller : Controller
    {
        private readonly AppDbContext _context;
        private readonly IContractRepository _contractrepository;
        private readonly IBlockRepository _blockchaintrepository;
        private readonly INodeRepository _nodeRepository;

        public Signcontroller(AppDbContext context, IContractRepository contractrepository, IBlockRepository blockchaintrepository, INodeRepository nodeRepository/*, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager*/)
        {
            _blockchaintrepository = blockchaintrepository;
            _contractrepository = contractrepository;
            _nodeRepository = nodeRepository;
            _context = context;
        }

        public static List<Block> ReceivedBlocks = new List<Block>();



        //test sign
        public IActionResult Index(int id)
        {
            var signViewModel = new SignViewModel();
            var contract = _contractrepository.GetContractById(id);
            int idNode = 1;
            try
            {
                var node = _nodeRepository.GetNodeById(idNode);
                string data = contract.Id + contract.Json + contract.Type + contract.Version + contract.Date + contract.ContractBody;
                var privateKey = node.PrivateKey;
                var publicKey = node.PublicKey;
                var signedMessage = PublicKeyAuth.Sign(data, privateKey);
                var hash = CryptoHash.Sha256(data);
                signViewModel.Hash = Convert.ToBase64String(hash);
                signViewModel.Signature = Convert.ToBase64String(signedMessage);
                signViewModel.KeyR = Convert.ToBase64String(privateKey);
                signViewModel.KeyU = Convert.ToBase64String(publicKey);
                signViewModel.Result = true;
                contract.Signature = Convert.ToBase64String(signedMessage);
                _context.Update(contract);
                _context.SaveChanges();
            }
            catch (Exception e)

            {
                signViewModel.Result = false;
                e.Message.ToString();
            }
            Startup.conn.Close();

            return View(signViewModel);
        }



        public IActionResult GenerateKeyPair(int IdNode)
        {
            IdNode = 1;
            {
                var signViewModel = new SignViewModel();
                var keyPair = PublicKeyAuth.GenerateKeyPair();
                var node = _nodeRepository.GetNodeById(IdNode);

                if (IdNode != node.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    signViewModel.Result = true;
                    try
                    {
                        node.PublicKey = keyPair.PublicKey;
                        node.PrivateKey = keyPair.PrivateKey;

                        signViewModel.KeyR = Convert.ToBase64String(keyPair.PrivateKey);
                        signViewModel.KeyU = Convert.ToBase64String(keyPair.PublicKey);

                        _context.Update(node);
                        _context.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        throw;
                    }
                }
                else { signViewModel.Result = false; }
                Startup.conn.Close();

                return View(signViewModel);
            }
        }


        //import key + certificate +  calculate hach 
        public IActionResult HashBlock(List<ContractBlock> contracts)
        {
            string path = @"C:\Users\Fatma\Desktop\privatekey.txt";
            //   string pathCert = @"C:\Users\Fatma\Desktop\certificate.crt";

            string privateKey = "";
            X509Certificate2 cert;
            int len;
            byte[] bytes = null; ;
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    privateKey = sr.ReadToEnd();
                }
                //using (StreamReader sr = new StreamReader(pathCert))
                //{
                //    cert = new X509Certificate2(sr.ReadToEnd());
                //}
                privateKey = privateKey.Substring(0, 64);
                len = privateKey.Length;
                //len = cert.GetPublicKey().Length;
            }
            catch (IOException e)
            {
                e.Message.ToString();
                throw;
            }

            bytes = Encoding.ASCII.GetBytes(privateKey);
            // string publicKey = cert.GetPublicKeyString();
            var encoding = new ASCIIEncoding();
            // string key = Convert.ToBase64String(encoding.GetBytes(publicKey));
            //preparing data to hash:
            List<int> idContracts = new List<int>();
            string ids = "";
            foreach (var contract in contracts)
            {
                idContracts.Add(contract.Id);
                ids = ids + contract.Id + ",";// list of contracts IDs: can separate  them with id[]= ids.split(",") ...
            }

            string prevHash = _blockchaintrepository.GetLastBlock().Hash;
            string data = prevHash + DateTime.Now + ids;
            string hash = GenericHash.Hash(data, bytes, 64).ToString();
            string hashString = Convert.ToBase64String(encoding.GetBytes(hash));
            string prevhashString = Convert.ToBase64String(encoding.GetBytes(prevHash));

            Block block = new Block
            {
                Date = DateTime.Now,
                NbrContracts = contracts.Count,
                Hash = hash,
                PreviousHash = prevHash
            };
            HashViewModel hashViewModel = new HashViewModel
            {
                Block = block,
                PrivateKey = privateKey,
                IdsContracts = ids
            };
            ReceivedBlocks.Add(block);
            return View(hashViewModel);
        }


        //verify the hash & pub key
        public List<Block> ValidateBlock()

        {
            List<Block> VerifiedBlocks = new List<Block>();

            int max = ReceivedBlocks.Count;
            if (max == 10)
            {
                for (int i = 0; i < ReceivedBlocks.Count; i++)
                {
                    byte[] hach = Encoding.ASCII.GetBytes(ReceivedBlocks[i].Hash);
                    byte[] key = Encoding.ASCII.GetBytes(ReceivedBlocks[i].PublicKey);
                    if ((PublicKeyAuth.Verify(hach, key).Length > 0))
                    {
                        //block non valide
                        VerifiedBlocks.Add(ReceivedBlocks[i]);

                    };
                }

            }

            return ReceivedBlocks;
        }



        //add the new block to the blockchain
        public IActionResult MinHash()
        {
             ValidateBlock();
            int l = ValidateBlock().Count;
            List<string> hashs = new List<string>();
            int indice = 0;
            foreach (var block in ValidateBlock())
            {
                hashs.Add(block.Hash.ToLower());
            }

            //  hashs.Sort();
            string min ="a";
            for (int i = 0; i < hashs.Count; i++)
            {
                if (hashs[i].CompareTo(min) < 0)
                {
                    min = hashs[i];
                    indice = i;
                }
            }
            //string s = System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            _context.AddRange(ValidateBlock()); //block with min Hash
            _context.SaveChanges();
            //select blick with min hash from  listBlocks sent by all nodes
           // ReceivedBlocks.Clear();

            HashViewModel hashViewModel = new HashViewModel
            {
                Block = ValidateBlock()[indice]
            };
            return View(hashViewModel);
        }




        //verify pwd by modal
        public IActionResult ImportKey(SignViewModel homeViewModel)
        {
            string recievedPwd = this.User.FindFirst("family_name")?.Value;
            string pwd = homeViewModel.Password;
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(pwd);
            byte[] hash = sha256.ComputeHash(bytes);
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            string Passwordhash = result.ToString();

            if (Passwordhash.Equals(recievedPwd.ToUpper()))
            {
                homeViewModel.Verified = true;
            }
            else
            {
                homeViewModel.Verified = false;
            }
            return View(homeViewModel);
        }


        public IActionResult AllReceivedBlocks()
        {
            HomeViewModel homeViewModel = new HomeViewModel
            {
                BlockChain = ReceivedBlocks
            };
            return View(homeViewModel);
        }



        public IActionResult Sign(string key)
        {
            string keypriv = System.IO.File.ReadAllText(key);
            string privateKey = "";
            X509Certificate2 cert;
            int len;
            byte[] bytes = null; ;
            bytes = Encoding.ASCII.GetBytes(privateKey);
            len = bytes.Length;
            var encoding = new ASCIIEncoding();
            string keyR = Convert.ToBase64String(encoding.GetBytes(key));
            //preparing data to sign:
            HomeViewModel homeViewModel = new HomeViewModel();
            return View(homeViewModel);
        }



    }
}


