﻿using BlockchaneContract.Models;
using BlockchaneContract.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlockchaneContract.Controllers
{

    public class NodesController : Controller
    {
        private readonly IBlockRepository _blockChainRepository;
        private readonly IContractRepository _contractRepository;
        public NodesController(IBlockRepository blockChainRepository, IContractRepository contractRepository)
        {
            _blockChainRepository = blockChainRepository;
            _contractRepository = contractRepository;
        }

        // GET: /<controller>/



        // GET: /<controller>/
        public IActionResult Status()
        {
            return View();
        }

        public IActionResult BlockChainDetails(Guid id)
        {
            Block block= _blockChainRepository.GetBlockChainById(id);
            if (block == null)
                return NotFound();
            block.Contracts = _contractRepository.GetContractsByBlock(id).ToList();
            return View(block);
        }



        public IActionResult BlockChainContracts(Guid id)
        {
            var contracts = _contractRepository.GetContractsByBlock(id);
            if (contracts == null)
                return NotFound();

            var homeViewModel = new HomeViewModel()
            {
                Title = contracts.Count().ToString(),
                Contracts = contracts.ToList()
            };

            return View(homeViewModel);

        }



        public IActionResult Blockchain()
        {
            var blockChain = _blockChainRepository.GetBlockChain().OrderBy(c => c.Date);
            var pending = _contractRepository.GetPendingContracts();
            var homeViewModel = new HomeViewModel()

            {
                Title = "BlockChain  ",
                BlockChain = blockChain.ToList(),
                Contracts=pending.ToList()
                
            };

            return View(homeViewModel);
        }

    }
}
