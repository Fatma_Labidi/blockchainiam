﻿using System.IO;
using System.Linq;
using BlockchaneContract.Models;
using BlockchaneContract.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BlockchaneContract.Controllers
{
   // [Authorize]
  //[ Route("api/[controller]")]
    public class HomeController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IContractRepository _ContractRepository;
        public HomeController(AppDbContext context, IContractRepository contractRepository)
        {
            _context = context;
            _ContractRepository = contractRepository;
        }
        public IActionResult Register()
        {
            return RedirectToAction("Register", "Account");
        }
        [Authorize]
        public IActionResult Contracts()
        {
            var contracts = _ContractRepository.GetAllContracts().OrderByDescending(c => c.Date);
            var homeViewModel = new HomeViewModel()
            {
                Title = "Pending contracts:  ",
                Contracts = contracts.ToList(),
                //QrCodeImage = qrCodeImage
            };
            return View(homeViewModel);
                        
                   }






        
        [HttpGet("api/home/ContractsApi")]
        public IActionResult ContractsApi()
        {
            var contracts = _ContractRepository.GetAllContracts().OrderByDescending(c => c.Date);

            return new JsonResult(contracts);

        }








       // [HttpGet("api/home/PendingContracts")]
        public IActionResult PendingContracts()
        {
            var pending = _ContractRepository.GetPendingContracts();
            //var homeViewModel = new HomeViewModel()
            //{
            //    Contracts = pending.ToList()
            //};

            return new JsonResult(pending);
        }
        

        [HttpGet("api/home/contractdetails/{id}")]
        public IActionResult ContractDetails(int id)
        {
            var contract = _ContractRepository.GetContractById(id);
            var homeViewModel = new HomeViewModel()
            {
                Contract = contract
            };
            return View(homeViewModel);
        }

        public IActionResult Queue()
        {
            var queue = _ContractRepository.GetQueue();
            var homeViewModel = new HomeViewModel()
            {
                Contracts = queue.ToList()
            };

            return View(homeViewModel);
        }


        public IActionResult ReceivedBlocks()
        {
            return View();
        }


        public IActionResult Identity()
        {
            return View();
        }


        public IActionResult ImportContract()
        {

            ContractBlock contract = JsonConvert.DeserializeObject<ContractBlock>(System.IO.File.ReadAllText("C:/Users/Fatma/Desktop/Contract.json"));
            _context.Add(contract);
           // _context.SaveChanges();

            HomeViewModel homeViewModel = new HomeViewModel { Contract = contract };
            return View(homeViewModel);
        }

    }
}
