﻿using System.Threading.Tasks;
using BlockchaneContract.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sodium;

namespace BlockchaneContract.Controllers
{
    public class AccountController : Controller
    {
        //private readonly SignInManager<IdentityUser> _signInManager;
        //private readonly UserManager<IdentityUser> _userManager;

        //dependencies injection, we don't need to add extra services they are bu default configurable 
        //public AccountController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        //{
        //    _signInManager = signInManager;
        //    _userManager = userManager;
        //}

        //LoginModal
        public IActionResult LoginModal()
        {
            return View();
        }

        //[HttpPost]
        //public async Task<IActionResult> LoginModal(LoginViewModel loginViewModel)
        //{
        //    if (!ModelState.IsValid)
        //        return View(loginViewModel);

        //    var user = await _userManager.FindByNameAsync(loginViewModel.UserName);

        //    if (user != null)
        //    {
                
        //        var result = await _signInManager.PasswordSignInAsync(user, loginViewModel.Password, false, false);
        //        ModelState.AddModelError("", result.ToString());
        //        if (result.Succeeded)
        //        {
        //            return RedirectToAction("Index", "Sign");
        //        }

        //        ModelState.AddModelError("", result.ToString());
        //    }

        //    ModelState.AddModelError("", "Validation failed ! ");
        //    return RedirectToAction("ContractDetails", "Home");
        //}








        public IActionResult Register()
        {
            return View();
        }

        //[HttpPost]
        //public async Task<IActionResult> Register(LoginViewModel loginViewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new IdentityUser() { UserName = loginViewModel.UserName };
        //        var result = await _userManager.CreateAsync(user, loginViewModel.Password);
        //        ModelState.AddModelError("", result.ToString());

        //        if (result.Succeeded)
        //        {
        //            return RedirectToAction("Login", "Account");
        //        }
        //    }
        //    return View(loginViewModel);
        //}


        ////method to test RegisterViewModel wich contains KeyPair: create keypare to every new user registred (by default) to test 
        //[HttpPost]
        //public async Task<IActionResult> Register2(RegisterViewModel registerViewModel)
        //{
        //    var keyPair = PublicKeyAuth.GenerateKeyPair();
        //    string key = keyPair.ToString();

        //    if (ModelState.IsValid)
        //    {
        //        var user = new IdentityUser() { UserName = registerViewModel.UserName};
        //      var result = await _userManager.CreateAsync(user, registerViewModel.Password);
        //       // IdentityResult res = await CreateAsync(user, registerViewModel.Password, key);

        //        ModelState.AddModelError("", result.ToString());

        //        if (result.Succeeded)
        //        {
        //            return RedirectToAction("Login", "Account");
        //        }
        //    }
        //    return View(registerViewModel);
        //}











        public IActionResult Login()
        {
            return View();
        }





        //[HttpPost]
        //public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        //{
        //    if (!ModelState.IsValid)
        //        return View(loginViewModel);

        //    var user = await _userManager.FindByNameAsync(loginViewModel.UserName);

        //    if (user != null)
        //    {

        //        var result = await _signInManager.PasswordSignInAsync(user, loginViewModel.Password, false, false);
        //        ModelState.AddModelError("", result.ToString());
        //        if (result.Succeeded)
        //        {



        //            return RedirectToAction("Contracts", "Home");

        //        }
        //        ModelState.AddModelError("", result.ToString());

        //    }

        //    ModelState.AddModelError("", "User name & password not found");
        //    return View(loginViewModel);
        //}


        //public IActionResult Register()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public async Task<IActionResult> Register(LoginViewModel loginViewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new IdentityUser() { UserName = loginViewModel.UserName };
        //        var result = await _userManager.CreateAsync(user, loginViewModel.Password);
        //        ModelState.AddModelError("", result.ToString());

        //        if (result.Succeeded)
        //        {
        //            return RedirectToAction("Login", "Account");
        //        }
        //    }
        //    return View(loginViewModel);
        //}



        public IActionResult SignIn()
        {
            return View();
        }

        //    [HttpPost]
        //    public async Task<IActionResult> SignIn(AccountViewModel accountViewModel)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var user = new IdentityUser() { UserName = accountViewModel.UserName, Email = accountViewModel.EmailAddress };
        //            var result = await _userManager.CreateAsync(user, accountViewModel.Password);
        //            ModelState.AddModelError("", result.ToString());

        //            if (result.Succeeded)
        //            {
        //                return RedirectToAction("Home", "Contracts");
        //            }
        //        }
        //        return View(accountViewModel);
        //    }


        //    [HttpPost]
        //    public async Task<IActionResult> Logout()
        //    {
        //        await _signInManager.SignOutAsync();
        //        return RedirectToAction("Login", "Account");
        //    }
           public async Task<IActionResult> Logout()
        {
        

            return  SignOut(new AuthenticationProperties
            {
                RedirectUri = ""
            }, "Cookies", "oidc");

        }


    }
}