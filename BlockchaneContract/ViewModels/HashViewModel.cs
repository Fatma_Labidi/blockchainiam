﻿using BlockchaneContract.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.ViewModels
{
    public class HashViewModel
    {
       
        public Block Block { get; set; }
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }
        public string IdsContracts { get; set; }
    }
}
