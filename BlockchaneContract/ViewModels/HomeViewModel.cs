﻿using BlockchaneContract.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace BlockchaneContract.ViewModels
{
    public class HomeViewModel
    {
        public List<ContractBlock> Contracts { get; set; }
        public List<Block> BlockChain{ get; set; }
        public List<Node> Nodes { get; set; }
        public ContractBlock Contract { get; set; }
        public string Signature { get; set; }
        public string Title { get; set; }
        public string Password { get; set; }
        public bool Verified { get; set; }
        public string Key { get; set; }
        public Bitmap QrCodeImage { get; set; }
    }
}
