﻿using BlockchaneContract.Models;
using System;

namespace BlockchaneContract.ViewModels
{
    public class SignViewModel
    {
        public bool Result { get; set; }
        public ContractBlock Contract { get; set; }
        public Node Node { get; set; }
        public Block BlockChain { get; set; }
        public Block Block { get; set; }
        public string Message { get; set; }
        public string Signature { get; set; }
        public string  Hash { get; set; }
        public string KeyU { get; set; }
        public string KeyR { get; set; }
        public int KeyPLength { get; set; }
        public int KeyPRength { get; set; }
        public bool Verified { get; set; }
        public string Password { get; set; }
        public FileStyleUriParser Key { get; set; }
    }
}
