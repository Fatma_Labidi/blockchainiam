﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BlockchaneContract.Migrations
{
    public partial class NullableIdBloc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "ForeignKey_ContractBlock_BlockChain",
                table: "Contracts");

            migrationBuilder.AlterColumn<Guid>(
                name: "IdBlock",
                table: "Contracts",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "ForeignKey_ContractBlock_BlockChain",
                table: "Contracts",
                column: "IdBlock",
                principalTable: "BlockChain",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "ForeignKey_ContractBlock_BlockChain",
                table: "Contracts");

            migrationBuilder.AlterColumn<Guid>(
                name: "IdBlock",
                table: "Contracts",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "ForeignKey_ContractBlock_BlockChain",
                table: "Contracts",
                column: "IdBlock",
                principalTable: "BlockChain",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
