﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BlockchaneContract.Migrations
{
    public partial class AddReceivedBlocks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_BlockChain",
                table: "BlockChain");

            migrationBuilder.RenameTable(
                name: "BlockChain",
                newName: "Block");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Block",
                table: "Block",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Block",
                table: "Block");

            migrationBuilder.RenameTable(
                name: "Block",
                newName: "BlockChain");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BlockChain",
                table: "BlockChain",
                column: "Id");
        }
    }
}
